package com.hpscds.refreshme;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Constants {

    private static final String PAIR_BACKGROUND_URL = "orange.jpg";
    private static final String MATHS_BACKGROUND_URL = "blue.png";
    private static final String COLORS_BACKGROUND_URL = "green.png";
    private static final String BACK_CARD_URL = "backcard.png";
    private static final String CIRCLE_CARD_URL = "circle.png";
    private static final String CUBE_CARD_URL = "cube.png";

    private static final String HEXAGON_CARD_URL = "hexagon.png";
    private static final String STAR_CARD_URL = "star.png";
    private static final String TRAINGLE_CARD_URL = "triangle.png";
    private static final String BLANK_CARD_URL = "cardblank.png";
    private static final String SCORE_URL = "score.png";
    private static final String PLACE_HOLDER_URL = "placeHolder.png";

    private static final String CIRCLE_ANIMATION_URL = "circleAnimation.atlas";
    private static final String CUBE_ANIMATION_URL = "cubeAnimation.atlas";
    private static final String HEXAGON_ANIMATION_URL = "hexagonAnimation.atlas";
    private static final String STAR_ANIMATION_URL = "starAnimation.atlas";
    private static final String TRIANGLE_ANIMATION_URL = "triangleAnimation.atlas";

    private static final String BACKGROUND_MUSIC_URL = "background_music.wav";
    private static final String CIRCLE_SOUND_URL = "circle_re.wav";
    private static final String CUBE_SOUND_URL = "cube_sol.wav";
    private static final String HEXAGON_SOUND_URL = "hexagon_la.wav";

    private static final String STAR_SOUND_URL = "star_fa.wav";
    private static final String TRIANGLE_SOUND_URL = "triangle_do.wav";
    private static final String SUCCESS_SOUND_URL = "success.wav";
    private static final String FAIL_SOUND_URL = "fail.wav";

    private static final String FONT_FNT_URL = "myFont.fnt";
    private static final String FONT_IMG_URL = "myFont.png";

    private static final float VIEWPORT_WIDTH = 16.0f;
    private static final float VIEWPORT_HEIGHT = 16.0f;

    private static final float DELAY_TIME_IN_SECONDS = 0.5f;
    private static final float ANIMATION_FRAME_DURATION = 0.12f;

    private static final float SCREEN_WIDTH = Gdx.graphics.getWidth();
    private static final float SCREEN_HEIGHT = Gdx.graphics.getHeight();

    private static final float TRANSFORMATION_RATIO = SCREEN_HEIGHT / SCREEN_WIDTH;

    private static final float CARD_WIDTH = 1.9f * TRANSFORMATION_RATIO;
    private static final float CARD_HEIGHT = 1.9f;

    private static final Vector2 POSITION_0 = new Vector2(-CARD_WIDTH - 0.6f * TRANSFORMATION_RATIO - (CARD_WIDTH / 2), -(CARD_HEIGHT / 2) + 1 + CARD_HEIGHT);
    private static final Vector2 POSITION_1 = new Vector2(-(CARD_WIDTH / 2), -(CARD_HEIGHT / 2) + 1 + CARD_HEIGHT);
    private static final Vector2 POSITION_2 = new Vector2(-(CARD_WIDTH / 2) + CARD_WIDTH + 0.6f * TRANSFORMATION_RATIO, -(CARD_HEIGHT / 2) + 1 + CARD_HEIGHT);
    private static final Vector2 POSITION_3 = new Vector2(-CARD_WIDTH - (0.6f * TRANSFORMATION_RATIO) - (CARD_WIDTH / 2), -(CARD_HEIGHT / 2));
    private static final Vector2 POSITION_4 = new Vector2(-(CARD_WIDTH / 2), -(CARD_HEIGHT / 2));
    private static final Vector2 POSITION_5 = new Vector2(-(CARD_WIDTH / 2) + CARD_WIDTH + (0.6f * TRANSFORMATION_RATIO), -(CARD_HEIGHT / 2));
    private static final Vector2 POSITION_6 = new Vector2(-CARD_WIDTH - 0.6f * TRANSFORMATION_RATIO - (CARD_WIDTH / 2), -(CARD_HEIGHT / 2) - 1 - CARD_HEIGHT);
    private static final Vector2 POSITION_7 = new Vector2(-(CARD_WIDTH / 2), -(CARD_HEIGHT / 2) - 1 - CARD_HEIGHT);
    private static final Vector2 POSITION_8 = new Vector2(-(CARD_WIDTH / 2) + CARD_WIDTH + 0.6f * TRANSFORMATION_RATIO, -(CARD_HEIGHT / 2) - 1 - CARD_HEIGHT);

    public static String getPairBackgroundUrl() {
        return PAIR_BACKGROUND_URL;
    }

    public static String getMathsBackgroundUrl() {
        return MATHS_BACKGROUND_URL;
    }

    public static String getColorsBackgroundUrl() {
        return COLORS_BACKGROUND_URL;
    }

    public static String getBackCardUrl() {
        return BACK_CARD_URL;
    }

    public static String getCircleCardUrl() {
        return CIRCLE_CARD_URL;
    }

    public static String getCubeCardUrl() {
        return CUBE_CARD_URL;
    }

    public static String getHexagonCardUrl() {
        return HEXAGON_CARD_URL;
    }

    public static String getStarCardUrl() {
        return STAR_CARD_URL;
    }

    public static String getTraingleCardUrl() {
        return TRAINGLE_CARD_URL;
    }

    public static String getBlankCardUrl() {
        return BLANK_CARD_URL;
    }

    public static String getScoreUrl() {
        return SCORE_URL;
    }

    public static String getPlaceHolderUrl() {
        return PLACE_HOLDER_URL;
    }

    public static String getStarAnimationUrl() {
        return STAR_ANIMATION_URL;
    }

    public static String getCircleAnimationUrl() {
        return CIRCLE_ANIMATION_URL;
    }

    public static String getCubeAnimationUrl() {
        return CUBE_ANIMATION_URL;
    }

    public static String getHexagonAnimationUrl() {
        return HEXAGON_ANIMATION_URL;
    }

    public static String getTriangleAnimationUrl() {
        return TRIANGLE_ANIMATION_URL;
    }

    public static String getBackgroundMusicUrl() {
        return BACKGROUND_MUSIC_URL;
    }

    public static String getCircleSoundUrl() {
        return CIRCLE_SOUND_URL;
    }

    public static String getCubeSoundUrl() {
        return CUBE_SOUND_URL;
    }

    public static String getHexagonSoundUrl() {
        return HEXAGON_SOUND_URL;
    }

    public static String getStarSoundUrl() {
        return STAR_SOUND_URL;
    }

    public static String getTriangleSoundUrl() {
        return TRIANGLE_SOUND_URL;
    }

    public static String getSuccessSoundUrl() {
        return SUCCESS_SOUND_URL;
    }

    public static String getFailSoundUrl() {
        return FAIL_SOUND_URL;
    }

    public static String getFontFntUrl() {
        return FONT_FNT_URL;
    }

    public static String getFontImgUrl() {
        return FONT_IMG_URL;
    }

    public static float getViewportWidth() {
        return VIEWPORT_WIDTH;
    }

    public static float getViewportHeight() {
        return VIEWPORT_HEIGHT;
    }

    public static float getDelayTimeInSeconds() {
        return DELAY_TIME_IN_SECONDS;
    }

    public static float getAnimationFrameDuration(){
        return ANIMATION_FRAME_DURATION;
    }

    public static Vector2 getPosition0() {
        return POSITION_0;
    }

    public static Vector2 getPosition1() {
        return POSITION_1;
    }

    public static Vector2 getPosition2() {
        return POSITION_2;
    }

    public static Vector2 getPosition3() {
        return POSITION_3;
    }

    public static Vector2 getPosition4() {
        return POSITION_4;
    }

    public static Vector2 getPosition5() {
        return POSITION_5;
    }

    public static Vector2 getPosition6() {
        return POSITION_6;
    }

    public static Vector2 getPosition7() {
        return POSITION_7;
    }

    public static Vector2 getPosition8() {
        return POSITION_8;
    }

    public static float getScreenWidth() {
        return SCREEN_WIDTH;
    }

    public static float getScreenHeight() {
        return SCREEN_HEIGHT;
    }

    public static float getTransformationRatio() {
        return TRANSFORMATION_RATIO;
    }

    public static float getCardWidth() {
        return CARD_WIDTH;
    }

    public static float getCardHeight() {
        return CARD_HEIGHT;
    }
}
