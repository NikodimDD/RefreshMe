package com.hpscds.refreshme.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.hpscds.refreshme.Constants;
import com.hpscds.refreshme.MyAssetsManager;
import com.hpscds.refreshme.colors.Colors;
import com.hpscds.refreshme.entities.ExitGameButton;
import com.hpscds.refreshme.entities.NextLevelButton;
import com.hpscds.refreshme.entities.RetryButton;
import com.hpscds.refreshme.matchPairs.MatchPairs;
import com.hpscds.refreshme.maths.Maths;

import java.util.ArrayList;

public class GameOverScreen extends AbstractScreen {

    private SpriteBatch batch;
    private OrthographicCamera camera;
    private MyAssetsManager assetManager = new MyAssetsManager();

    private Texture backgroundTexture;
    private Texture exitButtonTexture;
    private Texture retryButtonTexture;
    private Texture nextLevelButtonTexture;

    ExitGameButton exitButton;
    NextLevelButton nextLevelButton;
    RetryButton retryButton;

    private Vector2 clickPosition = new Vector2();
    private Vector3 cameraUnprojectCoordinates = new Vector3();

    private BitmapFont font;
    private ArrayList<Integer> score;
    private int highScore;


    private Maths maths;
    private Colors colors;
    private MatchPairs matchPairs;
    public AbstractScreen gameScreen;



    public GameOverScreen(Game game, ArrayList<Integer> gameResults) {
        super(game);
        score = gameResults;
        /*Get high score from file
        Preferences prefs = Gdx.app.getPreferences(game);
        highScore = prefs.getInteger("highscore",0);
        if(score > highScore){
            prefs.putInteger("highscore", 0);
            prefs.flush();
        }

         */
    }

    public String getGameName() {
        String completeClassName = String.valueOf(game.getClass());
        String gameName = "";
        String point = ".";
        int pointsCounter = 0;
        for (int i = 0; i < completeClassName.length(); i++) {
            if (point.equals(String.valueOf(completeClassName.charAt(i)))) {
                pointsCounter++;
            }
            if (pointsCounter == 4) {
                gameName = gameName + String.valueOf(completeClassName.charAt(i));
            }
        }
        gameName = gameName.replace(".", "");
        return gameName;
    }

    @Override
    public void show() {
        batch = new SpriteBatch();
        camera = new OrthographicCamera(Constants.getViewportWidth(), Constants.getViewportHeight());
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);

        assetManager.loadTextures();
        assetManager.manager.finishLoading();

        exitButton = new ExitGameButton(2, 2, new Vector2(3, 2));
        nextLevelButton = new NextLevelButton(2, 2, new Vector2(-1, 2));
        retryButton = new RetryButton(2, 2, new Vector2(-5, 2));

        backgroundTexture = assetManager.manager.get(Constants.getPairBackgroundUrl(), Texture.class);
        exitButtonTexture = new Texture(Gdx.files.internal("exitButton.png"));
        retryButtonTexture = new Texture(Gdx.files.internal("retryButton.png"));
        nextLevelButtonTexture = new Texture(Gdx.files.internal("nextLevelButton.png"));

        font = new BitmapFont(Gdx.files.internal("cambria.fnt"), Gdx.files.internal("cambria.png"), false);

        /**
         * PRUEBAS
         */
        maths = new Maths();
        colors = new Colors();
        matchPairs = new MatchPairs();

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        if(getGameName().equals("Colors")){
            batch.draw(backgroundTexture, -(Constants.getViewportWidth() / 2), -(Constants.getViewportHeight() / 2), Constants.getViewportWidth(), Constants.getViewportHeight());
            batch.draw(exitButtonTexture, exitButton.getPosition().x, exitButton.getPosition().y, exitButton.getWidth(), exitButton.getHeight());
            batch.draw(retryButtonTexture, retryButton.getPosition().x, retryButton.getPosition().y, retryButton.getWidth(), retryButton.getHeight());

            font.draw(batch, String.valueOf(score.get(0)), exitButton.getPosition().x - 1, exitButton.getPosition().y - 1, 10, 0, false);
            System.out.println("COLORS GAMEOVER POINTS: "+score);

            if (Gdx.input.justTouched()) {
                cameraUnprojectCoordinates = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
                clickPosition.set(cameraUnprojectCoordinates.x, cameraUnprojectCoordinates.y);
                if(retryButton.checkClick(clickPosition)){
                    System.out.println("RETRY BUTTON");
                    //llamar al game screen ya existente y luego tener un metodo que me vuelva a generar los colores
                    //gameScreen = new ColorsGameScreen(colors);
                    //game.setScreen(gameScreen);

                }
                else if(exitButton.checkClick(clickPosition)){
                    Gdx.app.exit();
                }
            }
        }
        else{
            batch.draw(backgroundTexture, -(Constants.getViewportWidth() / 2), -(Constants.getViewportHeight() / 2), Constants.getViewportWidth(), Constants.getViewportHeight());
            batch.draw(exitButtonTexture, exitButton.getPosition().x, exitButton.getPosition().y, exitButton.getWidth(), exitButton.getHeight());
            batch.draw(retryButtonTexture, retryButton.getPosition().x, retryButton.getPosition().y, retryButton.getWidth(), retryButton.getHeight());
            batch.draw(nextLevelButtonTexture, nextLevelButton.getPosition().x, nextLevelButton.getPosition().y, nextLevelButton.getWidth(), nextLevelButton.getHeight());
            System.out.println("MATHS/PAIRS GAMEOVER POINTS: "+score);
            if (Gdx.input.justTouched()) {
                cameraUnprojectCoordinates = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
                clickPosition.set(cameraUnprojectCoordinates.x, cameraUnprojectCoordinates.y);
                if(retryButton.checkClick(clickPosition)){
                    System.out.println("RETRY BUTTON");
                    if(getGameName().equals("Maths")){}
                    else {}
                }
                else if(nextLevelButton.checkClick(clickPosition)){
                    System.out.println("NEXT LEVEL BUTTON");
                    if(getGameName().equals("Maths")){}
                    else {}
                }
                else if(exitButton.checkClick(clickPosition)){
                    Gdx.app.exit();
                }
            }
        }
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
