package com.hpscds.refreshme.matchPairs;


import com.hpscds.refreshme.api.GameController;
import com.hpscds.refreshme.screens.AbstractScreen;


public class MatchPairs extends GameController {


    public AbstractScreen gameScreen;

    public MatchPairs(){}

    @Override
    public void create() {
        gameScreen = new MatchPairsGameScreen(this);
        setScreen(gameScreen);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
    }
}
