package com.hpscds.refreshme.api;

import com.badlogic.gdx.Game;

public abstract class GameController extends Game {
    /**
    * Lo he creado para que no moleste el constructor del juego
    * */
    public GameController() {

    }
}
