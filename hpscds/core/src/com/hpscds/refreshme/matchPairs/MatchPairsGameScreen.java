package com.hpscds.refreshme.matchPairs;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Timer;
import com.hpscds.refreshme.Constants;
import com.hpscds.refreshme.MyAssetsManager;
import com.hpscds.refreshme.entities.Card;
import com.hpscds.refreshme.entities.EntitiesFactory;
import com.hpscds.refreshme.screens.AbstractScreen;
import com.hpscds.refreshme.screens.GameOverScreen;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MatchPairsGameScreen extends AbstractScreen {

    public MatchPairs matchPairs;
    public AbstractScreen gameOverScreen;
    private MyAssetsManager assetManager = new MyAssetsManager();
    private SpriteBatch batch;
    private BitmapFont font;
    private OrthographicCamera camera;
    private ArrayList<Card> cards;

    private Texture background;
    private Texture blankCard;
    private Texture backCard;
    private Texture circleCard;
    private Texture cubeCard;
    private Texture hexagonCard;
    private Texture starCard;
    private Texture triangleCard;

    private Music backgroundMusic;
    private Sound succesSound;
    private Sound failSound;

    private Difficulty difficulty;
    private ArrayList<Card> turnedCards = new ArrayList<Card>();
    private Vector2 clickPosition = new Vector2();
    private Vector3 cameraUnprojectCoordinates = new Vector3();

    private int hits;
    private int failed;

    private ArrayList<Integer> points;

    public MatchPairsGameScreen(Game game) {
        super(game);
    }

    @Override
    public void show(){
        matchPairs = new MatchPairs();

        assetManager.loadTextures();
        assetManager.loadSounds();
        assetManager.manager.finishLoading();

        batch = new SpriteBatch();
        font = new BitmapFont();
        camera = new OrthographicCamera(Constants.getViewportWidth(), Constants.getViewportHeight());
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);

        difficulty = Difficulty.DIFFICULTY_HARD;
        cards = createGame(difficulty);

        background = assetManager.manager.get(Constants.getPairBackgroundUrl());
        blankCard = assetManager.manager.get(Constants.getBlankCardUrl());
        backCard = assetManager.manager.get(Constants.getBackCardUrl());
        circleCard = assetManager.manager.get(Constants.getCircleCardUrl());
        cubeCard = assetManager.manager.get(Constants.getCubeCardUrl());
        hexagonCard = assetManager.manager.get(Constants.getHexagonCardUrl());
        starCard = assetManager.manager.get(Constants.getStarCardUrl());
        triangleCard = assetManager.manager.get(Constants.getTraingleCardUrl());

        backgroundMusic = assetManager.manager.get(Constants.getBackgroundMusicUrl());
        backgroundMusic.setVolume(0.5f);
        backgroundMusic.setLooping(true);
        backgroundMusic.play();
        succesSound = assetManager.manager.get(Constants.getSuccessSoundUrl());
        failSound = assetManager.manager.get(Constants.getFailSoundUrl());

        hits = 0;
        failed = 0;

        points = new ArrayList<Integer>();
    }

    @Override
    public void render(float delta){
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, -(Constants.getViewportWidth() / 2), -(Constants.getViewportHeight() / 2), Constants.getViewportWidth(), Constants.getViewportHeight());

        if (Gdx.input.justTouched()) {
            cameraUnprojectCoordinates = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
            clickPosition.set(cameraUnprojectCoordinates.x, cameraUnprojectCoordinates.y);
            turnCard(clickPosition);
            if (checkEndGame()) {
                points.add(hits);
                points.add(failed);
                gameOverScreen = new GameOverScreen(matchPairs, points);
                dispose();
                game.setScreen(gameOverScreen);
            }
        }
        drawGame();
        batch.end();
    }

    /**
     *
     * Method used to create the game deck deppending on the dessired difficulty. Also  the pairs of figures are randomly setted on positions using Collections.shuffle().
     *
     * @param difficulty enum type value used to set game
     * @see Collections#shuffle(List)
     */
    private ArrayList<Card> createGame(Difficulty difficulty) {
        EntitiesFactory factory = new EntitiesFactory();
        ArrayList<Vector2> positions = new ArrayList<Vector2>();
        ArrayList<Integer> figures_id;
        ArrayList<Card> cards = new ArrayList<Card>();
        if (difficulty == null) {
            throw new IllegalArgumentException("Not identified difficulty for this argument.");
        } else {
            if (difficulty == Difficulty.DIFFICULTY_EASY) {
                figures_id = randomFigure(2);
                positions = generateDeckPosition(Difficulty.DIFFICULTY_EASY);
                Collections.shuffle(figures_id);
                for (int i = 0; i < positions.size(); i++) {
                    cards.add(factory.generateEntity(figures_id.get(i), positions.get(i)));
                }
                cards.add(factory.generateEntity(0, Constants.getPosition1()));
                cards.add(factory.generateEntity(0, Constants.getPosition3()));
                cards.add(factory.generateEntity(0, Constants.getPosition4()));
                cards.add(factory.generateEntity(0, Constants.getPosition5()));
                cards.add(factory.generateEntity(0, Constants.getPosition7()));
            } else if (difficulty == Difficulty.DIFFICULTY_MEDIUM) {
                figures_id = randomFigure(2);
                figures_id.add(figures_id.get(0));
                figures_id.add(figures_id.get(2));
                positions = generateDeckPosition(Difficulty.DIFFICULTY_MEDIUM);
                Collections.shuffle(figures_id);
                for (int i = 0; i < positions.size(); i++) {
                    cards.add(factory.generateEntity(figures_id.get(i), positions.get(i)));
                }
                cards.add(factory.generateEntity(0, Constants.getPosition3()));
                cards.add(factory.generateEntity(0, Constants.getPosition4()));
                cards.add(factory.generateEntity(0, Constants.getPosition5()));
            } else if (difficulty == Difficulty.DIFFICULTY_HIGHT) {
                figures_id = randomFigure(3);
                positions = generateDeckPosition(Difficulty.DIFFICULTY_HIGHT);
                Collections.shuffle(figures_id);
                for (int i = 0; i < positions.size(); i++) {
                    cards.add(factory.generateEntity(figures_id.get(i), positions.get(i)));
                }
                cards.add(factory.generateEntity(0, Constants.getPosition3()));
                cards.add(factory.generateEntity(0, Constants.getPosition4()));
                cards.add(factory.generateEntity(0, Constants.getPosition5()));
            } else if (difficulty == Difficulty.DIFFICULTY_HARD) {
                figures_id = randomFigure(4);
                positions = generateDeckPosition(Difficulty.DIFFICULTY_HARD);
                Collections.shuffle(figures_id);
                for (int i = 0; i < positions.size(); i++) {
                    cards.add(factory.generateEntity(figures_id.get(i), positions.get(i)));
                }
                cards.add(factory.generateEntity(0, Constants.getPosition4()));
            }
        }
        return cards;
    }

    /**
     * Randomly generate the pairs of figures we will use on the game, it need to know how many different type of figures will be used.
     *
     * @param number_of_figures number of different figures which will be used
     * @return                  array of integers which represents figures id's
     */
    private ArrayList<Integer> randomFigure(int number_of_figures) {
        int random_number;
        ArrayList<Integer> identifier = new ArrayList<Integer>();
        for (int i = 0; i < number_of_figures; i++) {
            random_number = (int) (Math.random() * 5) + 1;
            if (!identifier.contains(random_number)) {
                identifier.add(random_number);
                identifier.add(random_number);
            } else {
                while (identifier.contains(random_number)) {
                    random_number = (int) (Math.random() * 5) + 1;
                }
                identifier.add(random_number);
                identifier.add(random_number);
            }
        }
        return identifier;
    }

    /**
     * Generate the deck deppending on the difficulty, on less difficulty less generated positions. The deck position are predefined on Constants class.
     *
     * @param difficulty    level difficulty
     * @return              array list of type vector2 containing the positions where playable cards will be deployed
     */
    private ArrayList<Vector2> generateDeckPosition(Difficulty difficulty) {
        ArrayList<Vector2> positions = new ArrayList<Vector2>();
        if (difficulty == Difficulty.DIFFICULTY_EASY) {
            positions.add(Constants.getPosition0());
            positions.add(Constants.getPosition2());
            positions.add(Constants.getPosition6());
            positions.add(Constants.getPosition8());
            return positions;
        } else if (difficulty == Difficulty.DIFFICULTY_MEDIUM || difficulty == Difficulty.DIFFICULTY_HIGHT) {
            positions.add(Constants.getPosition0());
            positions.add(Constants.getPosition1());
            positions.add(Constants.getPosition2());
            positions.add(Constants.getPosition6());
            positions.add(Constants.getPosition7());
            positions.add(Constants.getPosition8());
            return positions;
        } else if (difficulty == Difficulty.DIFFICULTY_HARD) {
            positions.add(Constants.getPosition0());
            positions.add(Constants.getPosition1());
            positions.add(Constants.getPosition2());
            positions.add(Constants.getPosition3());
            positions.add(Constants.getPosition5());
            positions.add(Constants.getPosition6());
            positions.add(Constants.getPosition7());
            positions.add(Constants.getPosition8());
            return positions;
        }
        return null;
    }

    /**
     * Check if a card is clicked, this is checked by comparing the click coordinates with the area which different cards occupy.
     * @param clickPosition position where click was made
     * @return              boolean type, return true if card is not turned and it id is not 0 and return false on opposite
     */
    private boolean turnCard(Vector2 clickPosition) {
        for (int i = 0; i < cards.size(); i++) {
            if (clickPosition.x >= cards.get(i).getPosition().x &&
                    clickPosition.x <= (cards.get(i).getPosition().x + cards.get(i).getDimension().x) &&
                    clickPosition.y >= cards.get(i).getPosition().y &&
                    clickPosition.y <= (cards.get(i).getPosition().y + cards.get(i).getDimension().y)) {
                if (cards.get(i).getIsTouched() == false && cards.get(i).getId() != 0) {
                    cards.get(i).setIsTouched(true);
                    checkPairs(cards.get(i));
                }

                return true;
            }
        }
        return false;
    }

    /**
     * Method to check if two or three consecutive cards, deppending of level difficulty, are the correct cards.
     * @param card  check if card is first and must show it indeppendetly from it pair, and if it is not the first check if the before card is the same
     */
    private void checkPairs(final Card card) {
        if (difficulty == Difficulty.DIFFICULTY_MEDIUM) {
            if (turnedCards.isEmpty() || turnedCards.size() % 3 == 0) {
                succesSound.play();
                turnedCards.add(card);
            } else if (turnedCards.size() % 3 == 1) {
                if (card.getId() == turnedCards.get(turnedCards.size() - 1).getId()) {
                    succesSound.play();
                    turnedCards.add(card);
                    hits++;
                } else {
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            failSound.play();
                            card.setIsTouched(false);
                            turnedCards.get(turnedCards.size() - 1).setIsTouched(false);
                            turnedCards.remove(turnedCards.size() - 1);
                            failed++;
                        }
                    }, Constants.getDelayTimeInSeconds());
                }
            } else if (turnedCards.size() % 3 == 2) {
                if (card.getId() == turnedCards.get(turnedCards.size() - 1).getId()) {
                    succesSound.play();
                    turnedCards.add(card);
                } else {
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            failSound.play();
                            card.setIsTouched(false);
                            turnedCards.get(turnedCards.size() - 1).setIsTouched(false);
                            turnedCards.get(turnedCards.size() - 2).setIsTouched(false);
                            turnedCards.remove(turnedCards.size() - 1);
                            turnedCards.remove(turnedCards.size() - 1);
                            failed++;
                        }
                    }, Constants.getDelayTimeInSeconds());
                }
            }
        } else if (difficulty != Difficulty.DIFFICULTY_MEDIUM) {
            if (turnedCards.isEmpty() || turnedCards.size() % 2 == 0) {
                succesSound.play();
                turnedCards.add(card);
            } else if (turnedCards.size() % 2 == 1) {
                if (card.getId() == turnedCards.get(turnedCards.size() - 1).getId()) {
                    succesSound.play();
                    turnedCards.add(card);
                    hits++;
                } else {
                    Timer.schedule(new Timer.Task() {
                        @Override
                        public void run() {
                            failSound.play();
                            card.setIsTouched(false);
                            turnedCards.get(turnedCards.size() - 1).setIsTouched(false);
                            turnedCards.remove(turnedCards.size() - 1);
                            failed++;
                        }
                    }, Constants.getDelayTimeInSeconds());
                }
            }
        }
    }


    /**
     *
     * Method used to check if game is ended.
     * @return true if all cards are turned and game ends or return false on opposite case
     */
    private boolean checkEndGame() {
        int numberOfBlankCards = 0;
        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getId() == 0) {
                numberOfBlankCards++;
            }
        }
        if ((cards.size() - numberOfBlankCards) == turnedCards.size()) {
            return true;
        }
        return false;
    }

    /**
     *
     * Method used to draw each card on his correct position. This method will be called on each iteration of the render loop to update state of cards.
     */
    private void drawGame() {
        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getId() == 0) {
                batch.draw(blankCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
            } else if (cards.get(i).getId() == 1) {
                if (cards.get(i).getIsTouched() == true) {
                    batch.draw(circleCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                } else {
                    batch.draw(backCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                }
            } else if (cards.get(i).getId() == 2) {
                if (cards.get(i).getIsTouched() == true) {
                    batch.draw(cubeCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                } else {
                    batch.draw(backCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                }
            } else if (cards.get(i).getId() == 3) {
                if (cards.get(i).getIsTouched() == true) {
                    batch.draw(hexagonCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                } else {
                    batch.draw(backCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                }
            } else if (cards.get(i).getId() == 4) {
                if (cards.get(i).getIsTouched() == true) {
                    batch.draw(starCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                } else {
                    batch.draw(backCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                }
            } else if (cards.get(i).getId() == 5) {
                if (cards.get(i).getIsTouched() == true) {
                    batch.draw(triangleCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                } else {
                    batch.draw(backCard, cards.get(i).getPosition().x, cards.get(i).getPosition().y, cards.get(i).getDimension().x, cards.get(i).getDimension().y);
                }
            }
        }
    }


    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        assetManager.manager.dispose();
    }
}
