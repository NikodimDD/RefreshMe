package com.hpscds.refreshme.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class ExitGameButton extends GameButton {

    public ExitGameButton(int width, int height, Vector2 position) {
        super(width, height, position);
    }

    public boolean checkClick(Vector2 clickPosition) {
        if (clickPosition.x >= getPosition().x &&
                clickPosition.x <= (getPosition().x + getWidth()) &&
                clickPosition.y >= getPosition().y &&
                clickPosition.y <= (getPosition().y + getHeight())) {
            return true;
        }
        return false;
    }

}
