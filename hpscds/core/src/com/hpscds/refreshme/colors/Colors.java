package com.hpscds.refreshme.colors;


import com.hpscds.refreshme.api.GameController;
import com.hpscds.refreshme.screens.AbstractScreen;

/**
 * *****
 * Game where we must memorize the order of the showing figures.
 * @author Nikodim D. Dilkov
 */
public class Colors extends GameController {
    public AbstractScreen gameScreen;

    public Colors(){

    }

    @Override
    public void create() {
        gameScreen = new ColorsGameScreen(this);
        setScreen(gameScreen);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
    }

}
