package com.hpscds.refreshme.entities;

import com.badlogic.gdx.math.Vector2;

public class GameButton {

    private int width;
    private int height;
    private Vector2 position;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Vector2 getPosition() {
        return position;
    }

    public GameButton(int width, int height, Vector2 position) {
        this.width = width;
        this.height = height;
        this.position = position;
    }
}
