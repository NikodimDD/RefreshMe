package com.hpscds.refreshme;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class MyAssetsManager {

    public AssetManager manager = new AssetManager();

    public void loadTextures() {
        manager.load(Constants.getBackCardUrl(), Texture.class);
        manager.load(Constants.getBlankCardUrl(), Texture.class);
        manager.load(Constants.getCircleCardUrl(), Texture.class);
        manager.load(Constants.getCubeCardUrl(), Texture.class);
        manager.load(Constants.getHexagonCardUrl(), Texture.class);
        manager.load(Constants.getStarCardUrl(), Texture.class);
        manager.load(Constants.getTraingleCardUrl(), Texture.class);
        manager.load(Constants.getPairBackgroundUrl(), Texture.class);
        manager.load(Constants.getMathsBackgroundUrl(), Texture.class);
        manager.load(Constants.getColorsBackgroundUrl(), Texture.class);
        manager.load(Constants.getScoreUrl(), Texture.class);
        manager.load(Constants.getPlaceHolderUrl(), Texture.class);
    }

    public void loadAnimations(){
        manager.load(Constants.getCircleAnimationUrl(), TextureAtlas.class);
        manager.load(Constants.getCubeAnimationUrl() , TextureAtlas.class);
        manager.load(Constants.getHexagonAnimationUrl() , TextureAtlas.class);
        manager.load(Constants.getStarAnimationUrl() , TextureAtlas.class);
        manager.load(Constants.getTriangleAnimationUrl() , TextureAtlas.class);
    }

    public void loadSounds() {
        manager.load(Constants.getBackgroundMusicUrl(), Music.class);
        manager.load(Constants.getCircleSoundUrl(), Sound.class);
        manager.load(Constants.getCubeSoundUrl(), Sound.class);
        manager.load(Constants.getHexagonSoundUrl(), Sound.class);
        manager.load(Constants.getStarSoundUrl(), Sound.class);
        manager.load(Constants.getTriangleSoundUrl(), Sound.class);
        manager.load(Constants.getSuccessSoundUrl(), Sound.class);
        manager.load(Constants.getFailSoundUrl(), Sound.class);

    }
}
