package com.hpscds.refreshme.matchPairs;

public enum Difficulty {
    DIFFICULTY_EASY, DIFFICULTY_MEDIUM, DIFFICULTY_HIGHT, DIFFICULTY_HARD
}
