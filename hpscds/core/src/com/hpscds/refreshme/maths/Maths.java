package com.hpscds.refreshme.maths;

import com.hpscds.refreshme.api.GameController;
import com.hpscds.refreshme.screens.AbstractScreen;

public class Maths extends GameController {

    public AbstractScreen gameScreen;

    public Maths(){}

    @Override
    public void create() {
        gameScreen = new MathsGameScreen(this);
        setScreen(gameScreen);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
    }

}
