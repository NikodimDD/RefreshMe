package com.hpscds.refreshme.entities;

import com.badlogic.gdx.math.Vector2;

public class CardHexagon extends Card {

    /**
     * Class Attributes
     */
    private int id = 3;
    private String name = "hexagon";
    private boolean isTouched = false;

    /**
     * Class Methods
     */
    public int getId() {
        return id;
    }

    public String getName() {
        return this.name;
    }

    public void setIsTouched(boolean isTouched) {
        this.isTouched = isTouched;
    }

    public boolean getIsTouched() {
        return isTouched;
    }

    public CardHexagon(Vector2 position) {
        super(position);
    }
}
