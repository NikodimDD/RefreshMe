package com.hpscds.refreshme.maths;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.hpscds.refreshme.Constants;
import com.hpscds.refreshme.MyAssetsManager;
import com.hpscds.refreshme.entities.AnswerButton;
import com.hpscds.refreshme.matchPairs.Difficulty;
import com.hpscds.refreshme.screens.AbstractScreen;
import com.hpscds.refreshme.screens.GameOverScreen;

import java.util.ArrayList;
import java.util.Collections;

public class MathsGameScreen extends AbstractScreen {

    public Maths maths;
    public AbstractScreen gameOverScreen;
    private final MyAssetsManager assetManager = new MyAssetsManager();
    private SpriteBatch batch;
    private OrthographicCamera camera;
    private Vector2 clickPosition = new Vector2();
    private Vector3 cameraUnprojectCoordinates = new Vector3();

    private BitmapFont font;
    private Texture scoreTexture;
    private Texture placeHolderTexture;
    private Texture backgroundTexture;
    private Music backgroundMusic;
    private Sound failSound;
    private Sound successSound;

    private Difficulty difficulty;

    private AnswerButton answerButton1;
    private AnswerButton answerButton2;
    private AnswerButton answerButton3;
    private AnswerButton answerButton4;
    private AnswerButton answerButton5;
    private AnswerButton answerButton6;
    private AnswerButton answerButton7;
    private AnswerButton answerButton8;

    private AnswerButton[] answersArray;

    private int numberOfQuestions;
    private String scoreBoard;
    private int failed;
    private int hit;
    private int score;

    private boolean firstEntry;
    private int iterator;

    private ArrayList<Integer> points;
    private ArrayList<String[]> operationsVector;

    /**
     * Class constructor
     *
     * @param maths
     */
    public MathsGameScreen(Maths maths) {
        super(maths);
    }

    /**
     * Method to get the length of a number
     * @param number from which we get the length
     * */
    public int numberLength(int number) {
        int cyphers = 0;
        while (number != 0) {
            number = number / 10;
            cyphers++;
        }
        return cyphers;
    }

    /**
     * Mix the array of results so the correct is not placed always on same position.
     *
     * @param results array containing all the possible results
     * @return array with mixed results so the correct one is not always on same place
     */
    public int[] mixResults(int[] results) {
        ArrayList<Integer> aux = new ArrayList<Integer>();
        for (int i = 0; i < results.length; i++) {
            aux.add(results[i]);
        }
        Collections.shuffle(aux);
        for (int i = 0; i < results.length; i++) {
            results[i] = aux.get(i);
        }
        return results;
    }

    /**
     * Check if the elements is repeated in the results array, if repeated return his index, if it is not repeated return -1
     *
     * @param results array with all results to be checked if someone is repeated
     * @return if someresults is repeated his index is return
     */
    public int checkRepeatedResult(int[] results, int randomNumber) {
        int repeatedIndex = -1;
        int counter = 0;
        for (int i = 0; i < results.length; i++) {
            if (results[i] == randomNumber) {
                counter++;
                if (counter > 1) {
                    repeatedIndex = i;
                }
            }
        }
        return repeatedIndex;
    }

    /**
     * Method used to generate random results for an addition operation deppending on the setted difficulty
     * ID 0 -> addition
     * ID 1 -> substraction
     * ID 2 -> multiplication
     *
     * @param originalResult thats the good result of the operation used to generate the wrong answers
     * @param difficulty thats the difficulty used to generate answers
     * @return array with randomly generated wrong results
     */
    public int[] generateRandomResultAddition(int originalResult, Difficulty difficulty) {

        int[] resultEasy = new int[3];
        int[] resultDifficult = new int[5];

        if (difficulty == Difficulty.DIFFICULTY_EASY) {
            resultEasy[0] = originalResult;

            if ((int) (Math.random() * 2) + 1 == 1) {
                resultEasy[1] = originalResult + (int) (((Math.random() * 10) + 1) * (-1));
            } else {
                resultEasy[1] = originalResult + ((int) (Math.random() * 10) + 1);
            }
            resultEasy[2] = originalResult + ((int) (Math.random() * 10) + 1);
            while (resultEasy[1] == resultEasy[2]) {
                resultEasy[2] = originalResult + ((int) (Math.random() * 10) + 1);
            }
            return mixResults(resultEasy);
        }
        else if (difficulty == Difficulty.DIFFICULTY_MEDIUM) {
            resultEasy[0] = originalResult;

            if (numberLength(originalResult) == 1) {
                do {
                    if ((int) (Math.random() * 2) + 1 == 1) {
                        resultEasy[1] = originalResult + (int) (((Math.random() * 100) + 1) * (-1));
                    } else {
                        resultEasy[1] = originalResult + ((int) (Math.random() * 100) + 1);
                    }

                    resultEasy[2] = originalResult + ((int) (Math.random() * 10) + 1);
                    while (resultEasy[1] == resultEasy[2]) {
                        resultEasy[2] = originalResult + ((int) (Math.random() * 10) + 1);
                    }
                } while (numberLength(resultEasy[1]) != numberLength(originalResult) | numberLength(resultEasy[2]) > numberLength(originalResult));
                return mixResults(resultEasy);
            } else if (numberLength(originalResult) == 2) {

                do {
                    if ((int) (Math.random() * 2) + 1 == 1) {
                        resultEasy[1] = originalResult + (int) (((Math.random() * 100) + 1) * (-1));
                    } else {
                        resultEasy[1] = originalResult + ((int) (Math.random() * 100) + 1);
                    }

                    resultEasy[2] = originalResult + ((int) (Math.random() * 10) + 1);
                    while (resultEasy[1] == resultEasy[2]) {
                        resultEasy[2] = originalResult + ((int) (Math.random() * 10) + 1);
                    }
                } while (numberLength(resultEasy[1]) != numberLength(originalResult) | numberLength(resultEasy[2]) > numberLength(originalResult));
                return mixResults(resultEasy);
            } else if (numberLength(originalResult) == 3) {

                do {
                    if ((int) (Math.random() * 2) + 1 == 1) {
                        resultEasy[1] = originalResult + (int) (((Math.random() * 100) + 1) * (-1));
                    } else {
                        resultEasy[1] = originalResult + ((int) (Math.random() * 100) + 1);
                    }

                    resultEasy[2] = originalResult + ((int) (Math.random() * 10) + 1);
                    while (resultEasy[1] == resultEasy[2]) {
                        resultEasy[2] = originalResult + ((int) (Math.random() * 10) + 1);
                    }
                } while (numberLength(resultEasy[1]) != numberLength(originalResult) | numberLength(resultEasy[2]) > numberLength(originalResult));
                return mixResults(resultEasy);
            }
        }
        else if (difficulty == Difficulty.DIFFICULTY_HIGHT) {
                resultDifficult[0] = originalResult;
                if (numberLength(originalResult) == 1) {
                    for (int i = 1; i < resultDifficult.length; i++) {
                        do {
                            if ((int) (Math.random() * 2) + 1 == 1) {
                                resultDifficult[i] = originalResult + (int) (((Math.random() * 10) + 1) * (-1));
                            } else {
                                resultDifficult[i] = originalResult + ((int) (Math.random() * 10) + 1);
                            }

                        } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                    }
                    return mixResults(resultDifficult);
                }
                else if (numberLength(originalResult) == 2) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 100) + 1) * (-1));

                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 100) + 1);

                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            }
                else if (numberLength(originalResult) == 3) {

                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 100) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 100) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            }
            return mixResults(resultDifficult);
        }
        else if (difficulty == Difficulty.DIFFICULTY_HARD) {
            resultDifficult[0] = originalResult;

            if (numberLength(originalResult) == 1) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 10) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 10) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            } else if (numberLength(originalResult) == 2) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 100) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 100) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            }
            if (numberLength(originalResult) == 3) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 100) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 100) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            } else if (numberLength(originalResult) == 4) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 1000) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 1000) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            }
        }
        return null;
    }

    /**
     * Method used to generate random results for an substraction operation deppending on the setted difficulty
     * ID 0 -> addition
     * ID 1 -> substraction
     * ID 2 -> multiplication
     *
     * @param originalResult thats the good result of the operation used to generate the wrong answers
     * @param difficulty thats the difficulty used to generate answers
     * @return array with randomly generated wrong results
     */
    public int[] generateRandomResultSubstraction(int originalResult, Difficulty difficulty) {
        int[] resultEasy = new int[3];
        int[] resultDifficult = new int[5];

        if (difficulty == Difficulty.DIFFICULTY_EASY || difficulty == Difficulty.DIFFICULTY_MEDIUM) {
            resultEasy[0] = originalResult;

            if ((int) (Math.random() * 2) + 1 == 1) {
                resultEasy[1] = originalResult + (int) (((Math.random() * 100) + 1) * (-1));
            } else {
                resultEasy[1] = originalResult + ((int) (Math.random() * 100) + 1);
            }
            while (resultEasy[1] == resultEasy[2]) {
                resultEasy[2] = originalResult + ((int) (Math.random() * 100) + 1);
            }
            return mixResults(resultEasy);
        } else if (difficulty == Difficulty.DIFFICULTY_HIGHT) {
            resultDifficult[0] = originalResult;
            if (numberLength(originalResult) == 1) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 100) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 100) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            } else if (numberLength(originalResult) == 2) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 100) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 100) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            }
        } else if (difficulty == Difficulty.DIFFICULTY_HARD) {
            resultDifficult[0] = originalResult;

            if (numberLength(originalResult) == 1) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 10) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 10) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            } else if (numberLength(originalResult) == 2) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 10) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 10) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            } else if (numberLength(originalResult) == 3) {
                for (int i = 1; i < resultDifficult.length; i++) {
                    do {
                        if ((int) (Math.random() * 2) + 1 == 1) {
                            resultDifficult[i] = originalResult + ((int) ((Math.random() * 10) + 1) * (-1));
                        } else {
                            resultDifficult[i] = originalResult + ((int) (Math.random() * 10) + 1);
                        }
                    } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
                }
                return mixResults(resultDifficult);
            }
        }
        return null;
    }

    /**
     * The multiplications are already complicated by themselves, so in easy and medium there will be no restriction of the random numbers generated
     *while in high difficulty there will be a restriction that the results have the same number of digits as the original result, while
     *that in difficult, apart from the limitation of figures, a range of max 10 figures above or below the result will also be set.
     *
     * Method used to generate random results for an addition operation deppending on the setted difficulty
     * ID 0 -> addition
     * ID 1 -> substraction
     * ID 2 -> multiplication
     *
     * @param originalResult thats the good result of the operation used to generate the wrong answers
     * @param difficulty thats the difficulty used to generate answers
     * @return array with randomly generated wrong results
     */
    public int[] generateRandomResultMultiplication(int originalResult, Difficulty difficulty) {
        int[] resultEasy = new int[3];
        int[] resultDifficult = new int[5];

        if (difficulty == Difficulty.DIFFICULTY_EASY || difficulty == Difficulty.DIFFICULTY_MEDIUM) {
            resultEasy[0] = originalResult;

            if ((int) (Math.random() * 2) + 1 == 1) {
                resultEasy[1] = originalResult + ((int) ((Math.random() * 100) + 1) * (-1));
            } else {
                resultEasy[1] = originalResult + ((int) (Math.random() * 100) + 1);
            }
            resultEasy[2] = originalResult + ((int) (Math.random() * 100) + 1);
            while (resultEasy[1] == resultEasy[2]) {
                resultEasy[2] = originalResult + ((int) (Math.random() * 100) + 1);
            }
            return mixResults(resultEasy);
        } else if (difficulty == Difficulty.DIFFICULTY_HIGHT) {
            resultDifficult[0] = originalResult;

            for (int i = 1; i < resultDifficult.length; i++) {
                do {
                    if ((int) (Math.random() * 2) + 1 == 1) {
                        resultDifficult[i] = originalResult + ((int) ((Math.random() * 100) + 1) * (-1));
                    } else {
                        resultDifficult[i] = originalResult + ((int) (Math.random() * 100) + 1);
                    }
                } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
            }
            return mixResults(resultDifficult);
        } else if (difficulty == Difficulty.DIFFICULTY_HARD) {
            resultDifficult[0] = originalResult;

            for (int i = 1; i < resultDifficult.length; i++) {
                do {
                    if ((int) (Math.random() * 2) + 1 == 1) {
                        resultDifficult[i] = originalResult + ((int) ((Math.random() * 10) + 1) * (-1));
                    } else {
                        resultDifficult[i] = originalResult + ((int) (Math.random() * 10) + 1);
                    }
                } while (numberLength(resultDifficult[i]) != numberLength(originalResult) | checkRepeatedResult(resultDifficult, resultDifficult[i]) != -1);
            }
            return mixResults(resultDifficult);
        }
        return null;

    }

    /**
     * Generate random operations which return a vector formed by the two operands and result. Also call a method to generate random results for each operations, we pass
     * to this method the vector with the operands, result and the realized operation identified by it id.
     * ID 0 -> addition
     * ID 1 -> substraction
     * ID 2 -> multiplication
     *
     * @param difficulty
     */
    public String[] randomOperation(Difficulty difficulty) {
        String[] easyToString = new String[7];
        String[] hardToString = new String[9];
        int[] operationalElements = new int[3];
        int[] resultEasy = new int[3];
        int[] resultDifficult = new int[5];
        int rand = (int) (Math.random() * 3);
        if (rand == 0) {
            operationalElements = addition(difficulty);
            if (difficulty == Difficulty.DIFFICULTY_EASY || difficulty == Difficulty.DIFFICULTY_MEDIUM) {
                resultEasy = generateRandomResultAddition(operationalElements[2], difficulty);
                easyToString[0] = String.valueOf(operationalElements[0]);
                easyToString[1] = " + ";
                easyToString[2] = String.valueOf(operationalElements[1]);
                easyToString[3] = " = ";
                easyToString[4] = String.valueOf(resultEasy[0]);
                easyToString[5] = String.valueOf(resultEasy[1]);
                easyToString[6] = String.valueOf(resultEasy[2]);
                return easyToString;
            } else {
                resultDifficult = generateRandomResultAddition(operationalElements[2], difficulty);
                hardToString[0] = String.valueOf(operationalElements[0]);
                hardToString[1] = " + ";
                hardToString[2] = String.valueOf(operationalElements[1]);
                hardToString[3] = " = ";
                hardToString[4] = String.valueOf(resultDifficult[0]);
                hardToString[5] = String.valueOf(resultDifficult[1]);
                hardToString[6] = String.valueOf(resultDifficult[2]);
                hardToString[7] = String.valueOf(resultDifficult[3]);
                hardToString[8] = String.valueOf(resultDifficult[4]);
                return hardToString;
            }
        } else if (rand == 1) {
            operationalElements = substraction(difficulty);
            if (difficulty == Difficulty.DIFFICULTY_EASY || difficulty == Difficulty.DIFFICULTY_MEDIUM) {
                resultEasy = generateRandomResultSubstraction(operationalElements[2], difficulty);
                easyToString[0] = String.valueOf(operationalElements[0]);
                easyToString[1] = " - ";
                easyToString[2] = String.valueOf(operationalElements[1]);
                easyToString[3] = " = ";
                easyToString[4] = String.valueOf(resultEasy[0]);
                easyToString[5] = String.valueOf(resultEasy[1]);
                easyToString[6] = String.valueOf(resultEasy[2]);
                return easyToString;
            } else {
                resultDifficult = generateRandomResultSubstraction(operationalElements[2], difficulty);
                hardToString[0] = String.valueOf(operationalElements[0]);
                hardToString[1] = " - ";
                hardToString[2] = String.valueOf(operationalElements[1]);
                hardToString[3] = " = ";
                hardToString[4] = String.valueOf(resultDifficult[0]);
                hardToString[5] = String.valueOf(resultDifficult[1]);
                hardToString[6] = String.valueOf(resultDifficult[2]);
                hardToString[7] = String.valueOf(resultDifficult[3]);
                hardToString[8] = String.valueOf(resultDifficult[4]);
                return hardToString;
            }

        } else if (rand == 2) {
            operationalElements = multiplication(difficulty);
            if (difficulty == Difficulty.DIFFICULTY_EASY || difficulty == Difficulty.DIFFICULTY_MEDIUM) {
                resultEasy = generateRandomResultMultiplication(operationalElements[2], difficulty);
                easyToString[0] = String.valueOf(operationalElements[0]);
                easyToString[1] = " * ";
                easyToString[2] = String.valueOf(operationalElements[1]);
                easyToString[3] = " = ";
                easyToString[4] = String.valueOf(resultEasy[0]);
                easyToString[5] = String.valueOf(resultEasy[1]);
                easyToString[6] = String.valueOf(resultEasy[2]);
                return easyToString;

            } else {
                resultDifficult = generateRandomResultMultiplication(operationalElements[2], difficulty);
                hardToString[0] = String.valueOf(operationalElements[0]);
                hardToString[1] = " * ";
                hardToString[2] = String.valueOf(operationalElements[1]);
                hardToString[3] = " = ";
                hardToString[4] = String.valueOf(resultDifficult[0]);
                hardToString[5] = String.valueOf(resultDifficult[1]);
                hardToString[6] = String.valueOf(resultDifficult[2]);
                hardToString[7] = String.valueOf(resultDifficult[3]);
                hardToString[8] = String.valueOf(resultDifficult[4]);
                return hardToString;
            }
        }
        return null;
    }

    /**
     * Method used to generate random additions operations and his correct answer. Deppending on the difficulty the operands will have more or less digits.
     * @param difficulty difficulty parameter used to generate the operations
     * @return array composed of both operands and his result
     */
    public int[] addition(Difficulty difficulty) {
        int operandA = 0;
        int operandB = 0;
        int result = -1;
        int[] elements = new int[3];
        if (difficulty == Difficulty.DIFFICULTY_EASY) {
            operandA = (int) (Math.random() * 10);
            operandB = (int) (Math.random() * 10);
            result = operandA + operandB;
            elements[0] = operandA;
            elements[1] = operandB;
            elements[2] = result;
            return elements;
        } else if (difficulty == Difficulty.DIFFICULTY_MEDIUM) {
            while (result < 0 || result > 99) {
                operandA = (int) (Math.random() * 100);
                operandB = (int) (Math.random() * 10);
                result = operandA + operandB;
                elements[0] = operandA;
                elements[1] = operandB;
                elements[2] = result;

            }
            return elements;
        } else if (difficulty == Difficulty.DIFFICULTY_HIGHT) {
            while (result < 0 || result > 99) {
                operandA = (int) (Math.random() * 100);
                operandB = (int) (Math.random() * 100);
                result = operandA + operandB;
                elements[0] = operandA;
                elements[1] = operandB;
                elements[2] = result;
            }
            return elements;
        } else if (difficulty == Difficulty.DIFFICULTY_HARD) {
            operandA = (int) (Math.random() * 1000);
            operandB = (int) (Math.random() * 100);
            result = operandA + operandB;
            elements[0] = operandA;
            elements[1] = operandB;
            elements[2] = result;
            return elements;

        }
        return elements;
    }

    /**
     * Method used to generate random substractions operations and his correct answer. Deppending on the difficulty the operands will have more or less digits.
     * @param difficulty difficulty parameter used to generate the operations
     * @return array composed of both operands and his result
     */
    public int[] substraction(Difficulty difficulty) {
        int operandA = 0;
        int operandB = 0;
        int result = 0;
        int[] elements = new int[3];
        if (difficulty == Difficulty.DIFFICULTY_EASY) {
            operandA = (int) (Math.random() * 10);
            operandB = (int) (Math.random() * 10);
            if (operandA >= operandB) {
                result = operandA - operandB;
                elements[0] = operandA;
                elements[1] = operandB;
                elements[2] = result;
                return elements;
            } else {
                result = operandB - operandA;
                elements[0] = operandB;
                elements[1] = operandA;
                elements[2] = result;
                return elements;
            }
        } else if (difficulty == Difficulty.DIFFICULTY_MEDIUM) {
            operandA = (int) (Math.random() * 100);
            operandB = (int) (Math.random() * 10);
            result = operandA - operandB;
            elements[0] = operandA;
            elements[1] = operandB;
            elements[2] = result;
            return elements;
        } else if (difficulty == Difficulty.DIFFICULTY_HIGHT) {
            operandA = (int) (Math.random() * 100);
            operandB = (int) (Math.random() * 100);
            if (operandA >= operandB) {
                result = operandA - operandB;
                elements[0] = operandA;
                elements[1] = operandB;
                elements[2] = result;
                return elements;
            } else {
                result = operandB - operandA;
                elements[0] = operandB;
                elements[1] = operandA;
                elements[2] = result;
                return elements;
            }
        } else if (difficulty == Difficulty.DIFFICULTY_HARD) {
            operandA = (int) (Math.random() * 1000);
            operandB = (int) (Math.random() * 100);
            result = operandA - operandB;
            elements[0] = operandA;
            elements[1] = operandB;
            elements[2] = result;
            return elements;
        }
        return elements;
    }

    /**
     * Method used to generate random multiplications operations and his correct answer. Deppending on the difficulty the operands will have more or less digits.
     * @param difficulty difficulty parameter used to generate the operations
     * @return array composed of both operands and his result
     */
    public int[] multiplication(Difficulty difficulty) {
        int operandA, operandB, result = 0;
        int[] elements = new int[3];
        if (difficulty == Difficulty.DIFFICULTY_EASY) {
            operandA = (int) (Math.random() * 5) + 1;
            operandB = (int) (Math.random() * 5) + 1;
            result = operandA * operandB;
            elements[0] = operandA;
            elements[1] = operandB;
            elements[2] = result;
            return elements;

        } else if (difficulty == Difficulty.DIFFICULTY_MEDIUM) {
            operandA = (int) (Math.random() * 7) + 1;
            operandB = (int) (Math.random() * 7) + 1;
            result = operandA * operandB;
            elements[0] = operandA;
            elements[1] = operandB;
            elements[2] = result;
            return elements;
        } else if (difficulty == Difficulty.DIFFICULTY_HIGHT) {
            operandA = (int) (Math.random() * 9) + 1;
            operandB = (int) (Math.random() * 9) + 1;
            result = operandA * operandB;
            elements[0] = operandA;
            elements[1] = operandB;
            elements[2] = result;
            return elements;
        } else if (difficulty == Difficulty.DIFFICULTY_HARD) {
            operandA = (int) (Math.random() * 100) + 1;
            operandB = (int) (Math.random() * 6) + 1;
            result = operandA * operandB;
            elements[0] = operandA;
            elements[1] = operandB;
            elements[2] = result;
            return elements;
        }
        return elements;
    }

    @Override
    public void show() {
        maths = new Maths();
        batch = new SpriteBatch();
        camera = new OrthographicCamera(Constants.getViewportWidth(), Constants.getViewportHeight());
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);

        font = new BitmapFont(Gdx.files.internal("font.fnt"), Gdx.files.internal("font.png"), false);

        assetManager.loadTextures();
        assetManager.loadSounds();
        assetManager.manager.finishLoading();

        scoreTexture = assetManager.manager.get(Constants.getScoreUrl());
        placeHolderTexture = assetManager.manager.get(Constants.getPlaceHolderUrl());
        backgroundTexture = assetManager.manager.get(Constants.getMathsBackgroundUrl());
        backgroundMusic = assetManager.manager.get(Constants.getBackgroundMusicUrl());
        failSound = assetManager.manager.get(Constants.getFailSoundUrl());
        successSound = assetManager.manager.get(Constants.getSuccessSoundUrl());


        backgroundMusic.setVolume(1.0f);
        backgroundMusic.setLooping(true);
        backgroundMusic.play();

        difficulty = Difficulty.DIFFICULTY_HARD;

        answersArray = new AnswerButton[5];

        numberOfQuestions = 10;
        scoreBoard = "0";
        failed = 0;
        hit = 0;
        score = 0;
        points = new ArrayList<Integer>();
        operationsVector = new ArrayList<String[]>();

        firstEntry = true;
        iterator = 0;
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        scoreBoard = String.valueOf((Integer.parseInt(scoreBoard) + score));
        font.getData().setScale(0.056f, 0.026f);
        font.setColor(Color.BLACK);
        font.draw(batch, scoreBoard, -8, 7, 10, 0, false);
        game(difficulty);
        batch.end();
    }

    /**
     * Deppending on difficulty we create a set of operations which are stored into an arraylist
     * @param difficulty parameter used to generate the mathematical operations
     * @return an arraylist with all the mathematical operations which will be showed to the user
     */
    public ArrayList generateOperationsVector(Difficulty difficulty){
        ArrayList<String[]> operationsVector = new ArrayList<String[]>();
        for (int i = 0; i < numberOfQuestions; i ++){
            operationsVector.add(randomOperation(difficulty));
        }
        return operationsVector;
    }

    /**
     * Method used to get the equation to the "=" sign and use this to display it as question
     * @param completeString string composed with all the operands
     * @return a equiations string finished on "=" sign
     */
    public String getEquation(String[] completeString){
        String equation = "";
        for (int i = 0; i <= 3; i ++){
            equation = equation + completeString[i];
        }
        return equation;
    }

    /**
     * Gets the answers for each equiations, including good resilts and wrong results. This will be used to display on screen as options.
     * @param completeString string containing everything, equation and results
     * @param difficulty
     * @return array of strings of possible results
     */
    public String[] getAnswers(String[] completeString, Difficulty difficulty){
        String[] answersEasy = new String[3];
        String[] answersHard = new String[5];
        if(difficulty == Difficulty.DIFFICULTY_EASY || difficulty == Difficulty.DIFFICULTY_MEDIUM){
            for(int i = 4; i < 7; i ++){
                answersEasy[i-4] = completeString[i];

            }
            return answersEasy;
        } else if(difficulty == Difficulty.DIFFICULTY_HIGHT || difficulty == Difficulty.DIFFICULTY_HARD){
            for(int i = 4; i < 9; i ++){
                answersHard[i-4] = completeString[i];
            }
            return answersHard;
        }
        return null;
    }

    /**
     * This method draw on screen placeholders of where will be displayed equations and possible answers
     * @param difficulty used to know how many possible answers will be displayed 3 or 5
     * @param completeString complete string of equiation and answers
     */
    public void drawElements(Difficulty difficulty, String[] completeString){
        String equation;
        String[] resultsEasy = new String[3];
        String[] resultsHard = new String[5];
        System.out.println("La puntuacion es de:"+score);
        scoreBoard = Integer.toString(score);
        if(difficulty == Difficulty.DIFFICULTY_EASY || difficulty == Difficulty.DIFFICULTY_MEDIUM){
            batch.draw(backgroundTexture, -(Constants.getViewportWidth() / 2), -(Constants.getViewportHeight() / 2), Constants.getViewportWidth(), Constants.getViewportHeight());
            batch.draw(scoreTexture, -(Constants.getViewportWidth() / 2), Constants.getViewportHeight() / 3, Constants.getViewportWidth(), Constants.getViewportHeight() / 8);
            batch.draw(placeHolderTexture, -7, 1, 14, 3);
            batch.draw(placeHolderTexture, -7, -2, 4, 2);
            batch.draw(placeHolderTexture, -2, -2, 4, 2);
            batch.draw(placeHolderTexture, 3, -2, 4, 2);
            equation = getEquation(completeString);
            resultsEasy = getAnswers(completeString, difficulty);
            font.draw(batch, scoreBoard, -2,7);
            font.draw(batch, equation, -6, 3, 10, 0, false);
            font.draw(batch, resultsEasy[0], -13, -0.2f, 10, 0, false );
            font.draw(batch, resultsEasy[1], -8.5f, -0.2f, 10, 0, false );
            font.draw(batch, resultsEasy[2], -3.5f, -0.2f, 10, 0, false );
        }
        else if (difficulty == Difficulty.DIFFICULTY_HIGHT || difficulty == Difficulty.DIFFICULTY_HARD){
            batch.draw(backgroundTexture, -(Constants.getViewportWidth() / 2), -(Constants.getViewportHeight() / 2), Constants.getViewportWidth(), Constants.getViewportHeight());
            batch.draw(scoreTexture, -(Constants.getViewportWidth() / 2), Constants.getViewportHeight() / 3, Constants.getViewportWidth(), Constants.getViewportHeight() / 8);
            batch.draw(placeHolderTexture, -7, 1, 14, 3);
            batch.draw(placeHolderTexture, -8, -2, 5, 2);
            batch.draw(placeHolderTexture, -3, -2, 5, 2);
            batch.draw(placeHolderTexture, 2, -2, 5, 2);
            batch.draw(placeHolderTexture, -6, -5, 5, 2);
            batch.draw(placeHolderTexture, 0, -5, 5, 2);
            equation = getEquation(completeString);
            resultsHard = getAnswers(completeString, difficulty);
            font.draw(batch, scoreBoard, -2,7);
            font.draw(batch, equation, -4, 3, 10, 0, false );
            font.draw(batch, resultsHard[0], -13f, -0.2f, 10, 0, false );
            font.draw(batch, resultsHard[1], -8f, -0.2f, 10, 0, false );
            font.draw(batch, resultsHard[2], -3f, -0.2f, 10, 0, false );
            font.draw(batch, resultsHard[3], -11f, -4, 10, 0, false );
            font.draw(batch, resultsHard[4], -5f, -4, 10, 0, false );
        }
    }

    /**
     * Buttons object are created only once, on first iteration.
     * @param difficulty used to know how many buttons need to be created
     */
    public void createButtons(Difficulty difficulty){
        switch (difficulty) {
            case DIFFICULTY_EASY:
                answerButton1 = new AnswerButton(3, 2, new Vector2(-7, -2));
                answerButton2 = new AnswerButton(3, 2, new Vector2(-2, -2));
                answerButton3 = new AnswerButton(3, 2, new Vector2(3, -2));
                break;
            case DIFFICULTY_MEDIUM:
                answerButton1 = new AnswerButton(3, 2, new Vector2(-7, -2));
                answerButton2 = new AnswerButton(3, 2, new Vector2(-2, -2));
                answerButton3 = new AnswerButton(3, 2, new Vector2(3, -2));
                break;
            case DIFFICULTY_HIGHT:
                answerButton4 = new AnswerButton(3, 2, new Vector2(-7, -2));
                answerButton5 = new AnswerButton(3, 2, new Vector2(-2, -2));
                answerButton6 = new AnswerButton(3, 2, new Vector2(3, -2));
                answerButton7 = new AnswerButton(3, 2, new Vector2(-5, -5));
                answerButton8 = new AnswerButton(3, 2, new Vector2(1, -5));
                break;
            case DIFFICULTY_HARD:
                answerButton4 = new AnswerButton(3, 2, new Vector2(-7, -2));
                answerButton5 = new AnswerButton(3, 2, new Vector2(-2, -2));
                answerButton6 = new AnswerButton(3, 2, new Vector2(3, -2));
                answerButton7 = new AnswerButton(3, 2, new Vector2(-5, -5));
                answerButton8 = new AnswerButton(3, 2, new Vector2(1, -5));
                break;
        }
    }

    /**
     * Once we had created the buttons, we must set the answers values to each one.
     * @param difficulty used to know how many buttons will be setted
     * @param operationString complete string of equation and result from wich we get values for the buttons
     */
    public void setButtonsAnswers(Difficulty difficulty, String[] operationString){
        if (difficulty == Difficulty.DIFFICULTY_EASY || difficulty == Difficulty.DIFFICULTY_MEDIUM) {
            answerButton1.setNumber(operationString[4]);
            answerButton2.setNumber(operationString[5]);
            answerButton3.setNumber(operationString[6]);
            answersArray[0] = answerButton1;
            answersArray[1] = answerButton2;
            answersArray[2] = answerButton3;
        }
        else if (difficulty == Difficulty.DIFFICULTY_HIGHT || difficulty == Difficulty.DIFFICULTY_HARD) {
            answerButton4.setNumber(operationString[4]);
            answerButton5.setNumber(operationString[5]);
            answerButton6.setNumber(operationString[6]);
            answerButton7.setNumber(operationString[7]);
            answerButton8.setNumber(operationString[8]);
            answersArray[0] = answerButton4;
            answersArray[1] = answerButton5;
            answersArray[2] = answerButton6;
            answersArray[3] = answerButton7;
            answersArray[4] = answerButton8;
        }
    }

    /**
     * Main method, used to start the playing the game.
     * @param difficulty
     */
    public void game(Difficulty difficulty) {
        if(firstEntry){
            firstEntry = false;
            createButtons(difficulty);
            operationsVector = generateOperationsVector(difficulty);
        }
        else {
            if (iterator < numberOfQuestions) {
                setButtonsAnswers(difficulty, operationsVector.get(iterator));
                drawElements(difficulty, operationsVector.get(iterator));
                if (Gdx.input.justTouched()) {
                    cameraUnprojectCoordinates = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
                    clickPosition.set(cameraUnprojectCoordinates.x, cameraUnprojectCoordinates.y);
                    checkAnswerClick(clickPosition,operationsVector.get(iterator), difficulty);
                    iterator++;
                }
            }
            else {
                points.add(score);
                points.add(hit);
                points.add(failed);
                gameOverScreen = new GameOverScreen(maths, points);
                dispose();
                game.setScreen(gameOverScreen);
            }
        }
    }

    /**
     * Check clicked position and get the value of the clicked button to check if it is the correct answer.
     * @param clickPosition coordinates of screen where click was made
     * @param operationString get que equation to check if the selected answer corresponds to the correct one
     * @param difficulty
     */
    public void checkAnswerClick(Vector2 clickPosition, String[] operationString, Difficulty difficulty) {
        String answer;
        if(difficulty == Difficulty.DIFFICULTY_EASY || difficulty == Difficulty.DIFFICULTY_MEDIUM) {
            for (int i = 0; i < answersArray.length-2; i++) {
                if (clickPosition.x >= answersArray[i].getPosition().x &&
                        clickPosition.x <= (answersArray[i].getPosition().x + answersArray[i].getWidth()) &&
                        clickPosition.y >= answersArray[i].getPosition().y &&
                        clickPosition.y <= (answersArray[i].getPosition().y + answersArray[i].getHeight())) {
                    answer = answersArray[i].getNumber();
                    if (answer.equals(correctAnswer(operationString))) {
                        score = score + 10;
                        hit = hit++;
                        successSound.play();
                    } else {
                        score = score - 5;
                        failed = failed++;
                        failSound.play();
                    }
                }
            }
        }
        else {
            for (int i = 0; i < answersArray.length; i++) {
                if (clickPosition.x >= answersArray[i].getPosition().x &&
                        clickPosition.x <= (answersArray[i].getPosition().x + answersArray[i].getWidth()) &&
                        clickPosition.y >= answersArray[i].getPosition().y &&
                        clickPosition.y <= (answersArray[i].getPosition().y + answersArray[i].getHeight())) {
                    answer = answersArray[i].getNumber();
                    if (answer.equals(correctAnswer(operationString))) {
                        score = score + 10;
                        hit = hit++;
                        successSound.play();
                    } else {
                        score = score - 5;
                        failed = failed++;
                        failSound.play();
                    }
                }
            }
        }
    }

    /**
     * Method that calculated correct answer by casting strings to int
     * @param printer recieves like string the generated operation
     * @return  correct answer as string
     */
    public String correctAnswer(String[] printer) {
        for(int j=0;j<printer.length;j++){
        }
        String correctAnswer = "";
        int firstNumber = Integer.parseInt(printer[0]);
        int secondNumber = Integer.parseInt(printer[2]);
        String operand = printer[1];
        if (operand.equals(" + ")) {
            correctAnswer = String.valueOf(firstNumber + secondNumber);
            return correctAnswer;
        } else if (operand.equals(" - ")) {
            correctAnswer = String.valueOf(firstNumber - secondNumber);
            return correctAnswer;
        } else if (operand.equals(" * ")) {
            correctAnswer = String.valueOf(firstNumber * secondNumber);
            return correctAnswer;
        }
        return correctAnswer;
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        assetManager.manager.dispose();
    }

}
